pytest~=5.4
pycodestyle~=2.5
codecov~=2.0
pylint
pytest-cov~=2.8
six~=1.14
py~=1.8
names
